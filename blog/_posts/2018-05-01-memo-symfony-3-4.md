---
layout: post
title:  "Mémo Symfony 3.4"
description: mémo sur symphony 3.4 (Installer, CLI, doctrine, TWIG, route, ...)
date:   2018-05-01 22:50:16 +0200
categories: Mémo
image: assets/images/memoSymfony.jpg
---

[Photo by Luca Bravo on Unsplash](https://unsplash.com/photos/XJXWbfSo2f0?modal=%7B%22userId%22%3A%22TrMEfNqww7s%22%2C%22tag%22%3A%22CreditBadge%22%7D)


## Installer Symfony sous linux


[symfony installer](https://github.com/symfony/symfony-installer)

puis dans la console `symfony new monprojet 3.4`

ou

Depuis composer `composer create-project symfony/framework-standard-edition monprojet "3.4.*"`


* php bin/console assets:install


### Lancer le serveur

`php bin/console server:run`


### Vérifier l'hébergement
[ URL ](http://localhost:8000/config.php)


## Paramétrer
dans app/config.yml -> [secret token](http://nux.net/secret)

enregistrer les valeurs bdd et smtp dans app/parametre.yml

## Creer un Bundle

`php bin/console generate:bundle`

[Tuto sur OC](https://openclassrooms.com/courses/developpez-votre-site-web-avec-le-framework-symfony/utilisons-la-console-pour-creer-un-bundle-1)

### Problemes
* A la création  du bundle
    An error occurred when executing the "'cache:clear --no-warmup'" command:
    PHP Fatal error:  Uncaught Symfony\Component\Debug\Exception\ClassNotFoundException: Attempted to load class "AppBundle" from namespace "AppBundle".
    Did you forget a "use" statement for another namespace? in /home/shoooryuken/www/blogOldGeek/app/AppKernel.php:19

1. Modifier composer.json
2. "organisation name\\namespace": "chemin/vers/bundle"  ex => "og\\blogBundle\\": "src/og/blogBundle" ou selon "tchatBundle\\": "src/tchatBundle"
3. composer --dump-autoload
4. composer update

* A la visite de  (http://localhost:8000/app_dev.php/)
    Unable to find template "ogblogBundle/Default/index.html.twig" (looked into: /home/shoooryuken/www/og/app/Resources/views, /home/shoooryuken/www/og/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form).

1. dans le bundle, ouvrir le default controller et remplacer

`return $this->render('ogblogBundle:Default:index.html.twig');`

par

`return $this->render('@ogblog/Default/index.html.twig');`


### Connaitre le nom du namespace

`bin/console debug:twig`


## Base de donnée

### Creer la bdd
`php bin/console doctrine:database:create`

### Doctrine

* Générer les entity

`php bin/console doctrine:generate:entity`

[liste de type géré par doctrine](https://www.doctrine-project.org/projects/doctrine-dbal/en/2.7/reference/types.html)

* Voir les requêtes en préparation

`php bin/console doctrine:schema:update --dump-sql`

* Générer les requêtes

`php bin/console doctrine:schema:update --force`


* Mettre à jour les entités

`php bin/console doctrine:schema:update`


* gestion ontToONe, manyToOne, ManyToMany

[lier 2 tables - tuto sur OC](https://openclassrooms.com/courses/developpez-votre-site-web-avec-le-framework-symfony2/les-relations-entre-entites-avec-doctrine2)


## Formulaires

* générer des formulaire bs3 ou 4

dans app/config/config.yml
{% highlight twig %}
twig:
    form_themes:
        - 'bootstrap_3_layout.html.twig'
{% endhighlight %}

* datepicker
https://knpuniversity.com/screencast/symfony-forms/date-picker-field

* formulaire many to many

[formulaire ManyToMany sur OC](https://openclassrooms.com/courses/developpez-votre-site-web-avec-le-framework-symfony/creer-des-formulaires-avec-symfony)
 -> rechercher collection / categorie

## Routing

* requirement = contraintes
exemple

{% highlight twig %}
requirements:
    year:   \d{4} #4 chiffre
    format: html|xml #que html ou xml
    page: \d*
{% endhighlight %}


* parametre par defaut (par defaut le format est html)

{% highlight twig %}
exemple
defaults:
    format:      html
{% endhighlight %}

* exemple la page admin est accessible qu'avec un numero de page (requirement \d* que des chiffres) et si il n'est pas renseigner c'est la page 1

{% highlight ruby %}
tchat_Admin:
path:     /admin/{page}/
defaults:
    controller: tchatBundle:Admin:index
    page: 1
requirements:
    page: \d*
{% endhighlight %}


## Twig

* Gérer un layout

en debut des fichiers ` extends "@nameSpace/layout.html.twig" `  avec des moustaches%
et générer layout.html.Twig

* ` dump() ` avec des doubles moustaches


* recupère un path avec un slug/id...

` path('tchat_Adminconversation', {'id': post.id}) `  avec des doubles moustaches



## Astuces

[Pagination tuto 6ma](http://www.6ma.fr/tuto/une+pagination+performante+avec+symfony-777)


[controller add, edit, delete](https://www.digitalocean.com/community/tutorials/how-to-use-symfony2-to-perform-crud-operations-on-a-vps-part-2)

ou mieux `php bin/console doctrine:generate:crud monBundle:entity`


[Générer un crud via doctrine](https://symfony.com/doc/master/bundles/SensioGeneratorBundle/commands/generate_doctrine_crud.html`)
